package com.sda.builder;

public class CustomerDemo {
    public static void main(String[] args) {
        Customer firstCustomer = new Customer.Builder(Gender.MASCULINE,"Salvatore","Stefan").build();
        Customer secondCustomer = new Customer.Builder(Gender.MASCULINE,"Salvatore","Damon").
                middleName("Vampire").build();
        Customer thirdCustomer = new Customer.Builder(Gender.FEMININE,"Gillbert","Elena").build();
        System.out.println(firstCustomer.getFirstName());
    }
}
