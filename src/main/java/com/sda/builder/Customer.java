package com.sda.builder;

import java.util.Date;

public class Customer {
    Gender gender;
    String surName;
    String firstName;
    String MiddleName;
    Date birthday;
    Date becomeCustomer;

    public Gender getGender() {
        return gender;
    }

    public String getSurName() {
        return surName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Date getBecomeCustomer() {
        return becomeCustomer;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "gender=" + gender +
                ", surName='" + surName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", MiddleName='" + MiddleName + '\'' +
                ", birthday=" + birthday +
                ", becomeCustomer=" + becomeCustomer +
                '}';
    }

    private Customer(Builder builder) {
        this.gender = builder.gender;
        this.surName = builder.surName;
        this.firstName = builder.firstName;
        this.MiddleName = builder.middleName;
        this.birthday = builder.birthday;
        this.becomeCustomer = builder.becomeCustomer;
    }

    public static class Builder{
        private Gender gender;
        private String surName;
        private String firstName;
        private String middleName;
        private Date birthday;
        private Date becomeCustomer;

        public Builder (Gender gender, String surName, String firstName){
            this.gender = gender;
            this.surName = surName;
            this.firstName = firstName;
        }

        public Builder middleName (String middleName){
            this.middleName = middleName;
            return this;
        }

        public Builder birthday (Date birthday){
            this.birthday = birthday;
            return this;
        }
        public Builder becomeCustomer (Date becomeCustomer){
            this.becomeCustomer = becomeCustomer;
            return this;
        }

        public Customer build(){return new Customer(this);}


    }
}
